package com.elielego.gbmnewsfeed.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.bluelinelabs.logansquare.annotation.JsonField;
import com.bluelinelabs.logansquare.annotation.JsonObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Eliel Martinez on 7/23/15.
 */
@JsonObject
public class News implements Parcelable {
    @JsonField
    private String title;
    @JsonField
    private String kwic;
    @JsonField
    private String content;
    @JsonField
    private String url;
    @JsonField
    private String iurl;
    @JsonField
    private String domain;
    @JsonField
    private String author;
    @JsonField
    private boolean news;
    @JsonField
    private int votes;
    @JsonField
    private double date;
    @JsonField
    private List<String> related;


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getKwic() {
        return kwic;
    }

    public void setKwic(String kwic) {
        this.kwic = kwic;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getIurl() {
        return iurl;
    }

    public void setIurl(String iurl) {
        this.iurl = iurl;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public boolean isNews() {
        return news;
    }

    public void setNews(boolean news) {
        this.news = news;
    }

    public int getVotes() {
        return votes;
    }

    public void setVotes(int votes) {
        this.votes = votes;
    }

    public double getDate() {
        return date;
    }

    public void setDate(double date) {
        this.date = date;
    }

    public List<String> getRelated() {
        return related;
    }

    public void setRelated(List<String> related) {
        this.related = related;
    }

    public News(){

    }

    /**
     * Constructor reconstruct from Parcel
     * @param source
     */

    public News(Parcel source){

        setTitle(source.readString());
        setKwic(source.readString());
        setContent(source.readString());
        setUrl(source.readString());
        setIurl(source.readString());
        setDomain(source.readString());
        setAuthor(source.readString());
        setNews(source.readByte() != 0);
        setVotes(source.readInt());
        setDate(source.readDouble());
        related = new ArrayList<String>();

        source.readList(related,getClass().getClassLoader());
    }

    public static final Parcelable.Creator<News> CREATOR
            = new Parcelable.Creator<News>()
    {
        public News createFromParcel(Parcel in)
        {

            return new News(in);
        }

        public News[] newArray (int size)
        {
            return new News[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(getTitle());
        dest.writeString(getKwic());
        dest.writeString(getContent());
        dest.writeString(getUrl());
        dest.writeString(getIurl());
        dest.writeString(getDomain());
        dest.writeString(getAuthor());
        dest.writeByte((byte) (isNews() ? 1 : 0));
        dest.writeInt(getVotes());
        dest.writeDouble(getDate());
        dest.writeList(getRelated());

    }
}
