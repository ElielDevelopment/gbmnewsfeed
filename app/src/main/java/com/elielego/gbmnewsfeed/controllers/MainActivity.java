package com.elielego.gbmnewsfeed.controllers;

import android.content.IntentFilter;
import android.os.Build;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.elielego.gbmnewsfeed.R;
import com.elielego.gbmnewsfeed.adapters.NewsAdapter;
import com.elielego.gbmnewsfeed.comunication.NewsDownloadProcess;
import com.elielego.gbmnewsfeed.comunication.PetitionService;
import com.elielego.gbmnewsfeed.faccade.Faccade;
import com.elielego.gbmnewsfeed.models.News;

import java.util.List;


public class MainActivity extends ActionBarActivity implements NewsDownloadProcess {

    private Faccade faccade;


    private RecyclerView recyclerNews;
    private RecyclerView.Adapter recyclerNewsAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private ProgressBar loadingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            overridePendingTransition(R.anim.activity_open_translate, R.anim.activity_close_scale);
        }

        recyclerNews = (RecyclerView) findViewById(R.id.recycler_news);
        loadingBar = (ProgressBar) findViewById(R.id.loading_bar);



        mLayoutManager = new LinearLayoutManager(this);
        recyclerNews.setLayoutManager(mLayoutManager);





        init();
    }

    private void init(){
        loadingBar.setVisibility(View.VISIBLE);
        faccade = new Faccade(this);
        faccade.delegate = this;
        faccade.getNews("http://hacoga.com/newsjson.php");
    }




    @Override
    public void onPause() {
        super.onPause();
        if (faccade != null)
            unregisterReceiver(faccade.receiver);

        //closing transition animations
        /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.FROYO) {
            overridePendingTransition(R.anim.activity_open_scale, R.anim.activity_close_translate);
        }*/

    }

    @Override
    public void onResume() {
        super.onResume();
        if (faccade != null)
            registerReceiver(faccade.receiver, new IntentFilter(PetitionService.INTENT_RECEIVER));

    }


    @Override
    public void processDownloadedNews(List<News> news) {
        recyclerNewsAdapter = new NewsAdapter(news, this);
        recyclerNews.setAdapter(recyclerNewsAdapter);
        loadingBar.setVisibility(View.INVISIBLE);
    }


}
