package com.elielego.gbmnewsfeed.comunication;

import android.app.Activity;
import android.app.IntentService;
import android.content.Intent;
import android.content.Context;

import com.bluelinelabs.logansquare.LoganSquare;
import com.elielego.gbmnewsfeed.models.News;
import com.elielego.gbmnewsfeed.models.Results;

import java.io.IOException;
import java.util.ArrayList;

/**
 * An {@link IntentService} subclass for handling asynchronous petitions in
 * a service on a separate handler thread.
 */
public class PetitionService extends IntentService {
    public static final String ACTION_DOWNLOAD = "com.elielego.gbmnewsfeed.comunication.ACTION_DOWNLOAD";

    private static final String EXTRA_URL = "com.elielego.gbmnewsfeed.comunication.extra.URL";


    public static final String RESULT = "result";
    public static final String ACTION_EXECUTED = "action_executed";
    public static final String EXTRA_DATA = "extra_data";
    public static final String INTENT_RECEIVER = "com.elielego.gbmnewsfeed.comunication.service.download.news.receiver";
    String action;


    /**
     * Starts this service to perform action Download with the given url. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */

    public static void startActionDownload(Context context, String url) {
        Intent intent = new Intent(context, PetitionService.class);
        intent.setAction(ACTION_DOWNLOAD);
        intent.putExtra(EXTRA_URL, url);
        context.startService(intent);
    }



    public PetitionService() {
        super("DownloadNewsService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            action = intent.getAction();
            if (ACTION_DOWNLOAD.equals(action)) {
                final String url = intent.getStringExtra(EXTRA_URL);
                handleActionDownload(url);
            }
        }
    }

    /**
     * * Handle action Download in the provided background thread with the provided
     * url.
     * @param url
     */
    private void handleActionDownload(String url) {
        HttpInvoker httpInvoker = new HttpInvoker(this,this);
        httpInvoker.setUrl(url);
        httpInvoker.execute("");
    }



    /**
     * Publish Petition Results to a Broadcast Receiver defined in
     * some Activity or Fragment
     * @param result The result of the operation
     * @param data Result Data of the petition
     */
    public void publishResults(int result, String data) throws IOException {
        Intent intent = new Intent(PetitionService.INTENT_RECEIVER);
        intent.putExtra(PetitionService.RESULT, result);
        intent.putExtra(PetitionService.ACTION_EXECUTED, action);
        Results results = LoganSquare.parse(data, Results.class);
        ArrayList<News> newsArrayList = (ArrayList<News>) results.getResults();
        intent.putParcelableArrayListExtra(PetitionService.EXTRA_DATA,newsArrayList);
        //intent.putExtra(PetitionService.EXTRA_DATA, data);
        sendBroadcast(intent);
    }


}
