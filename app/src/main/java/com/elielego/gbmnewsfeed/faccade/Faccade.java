package com.elielego.gbmnewsfeed.faccade;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.elielego.gbmnewsfeed.comunication.NewsDownloadProcess;
import com.elielego.gbmnewsfeed.comunication.PetitionService;
import com.elielego.gbmnewsfeed.models.News;

import java.util.ArrayList;

/**
 * Created by Eliel Martínez on 7/23/15.
 *
 */
public class Faccade {
    private Context context;
    public NewsDownloadProcess delegate = null;

    public Faccade(Context context){
        this.context = context;
    }

    public void getNews(String url){
        PetitionService.startActionDownload(context,url);
    }

    public BroadcastReceiver receiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {

                int resultCode = bundle.getInt(PetitionService.RESULT);
                if (resultCode == Activity.RESULT_OK) {
                    String executed_action = bundle.getString(PetitionService.ACTION_EXECUTED);
                    if (executed_action.equals(PetitionService.ACTION_DOWNLOAD)) {
                        ArrayList<News> data = bundle.getParcelableArrayList(PetitionService.EXTRA_DATA);

                        delegate.processDownloadedNews(data);
                    }

                } else if (resultCode == Activity.RESULT_CANCELED){

                }
            }
        }
    };
}
